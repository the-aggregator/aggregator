package com.costin.aggregator.serde;

import com.costin.model.Metric;
import org.springframework.kafka.support.serializer.JsonSerde;

public class MetricJsonSerde extends JsonSerde<Metric> {
}
