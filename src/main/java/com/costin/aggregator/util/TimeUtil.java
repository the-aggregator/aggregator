package com.costin.aggregator.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TimeUtil {

    private TimeUtil() { }

    public static String fromTimestampToDateTimeWithMinutePrecision(String timestamp) {
        return fromTimestampToDateTimeWithMinutePrecision(Long.parseLong(timestamp));
    }

    public static String fromTimestampToDateTimeWithMinutePrecision(Long timestamp) {
        var localDateTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(timestamp),
                ZoneId.of("UTC")
        );
        return localDateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy'T'hh:mm"));
    }
}
