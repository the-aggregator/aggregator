package com.costin.aggregator;

import com.costin.model.Aggregate;
import com.costin.model.CountAndSum;
import com.costin.model.Metric;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.function.Function;

import static com.costin.aggregator.util.TimeUtil.fromTimestampToDateTimeWithMinutePrecision;

@SpringBootApplication
public class AggregatorApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(AggregatorApplication.class);

    private static final String DELIMITER = "###";

    @Bean
    public Function<KStream<String, Metric>, KStream<String, Aggregate>> process() {

        return input -> input
                .peek((k, v) -> LOGGER.info("Key: {} Value: {}", k, v))
                .groupBy(
                        (k, v) -> k + DELIMITER + fromTimestampToDateTimeWithMinutePrecision(v.getTimestamp()),
                        Grouped.with(Serdes.String(), new JsonSerde<>(Metric.class))
                ) // serverName###minute
                .aggregate(
                        CountAndSum::new, this::collect,
                        Materialized.with(Serdes.String(), new JsonSerde<>(CountAndSum.class))
                )
                .toStream()
                .map(this::createAggregate);
    }

    private CountAndSum collect(String key, Metric value, CountAndSum agg) {
        if (value.getTimestamp() <= agg.getTimestamp()) {
            return agg;
        }
        var count = agg.getCount() + 1;
        var sum = agg.getSum() + value.getValue();
        return new CountAndSum(count, sum, value.getTimestamp());
    }

    public KeyValue<String, Aggregate> createAggregate(String key, CountAndSum countAndSum) {
        var average = countAndSum.getSum() / (double) countAndSum.getCount();
        var minute = fromTimestampToDateTimeWithMinutePrecision(countAndSum.getTimestamp());
        var serverName = key.split(DELIMITER)[0];
        var aggregate = new Aggregate(
                serverName,
                minute,
                countAndSum.getCount(),
                countAndSum.getSum(),
                average
        );
        return KeyValue.pair(serverName, aggregate);
    }

    @Bean
    public Serde<Metric> metricSerde() {
        return new JsonSerde<>();
    }

    public static void main(String[] args) {
        SpringApplication.run(AggregatorApplication.class, args);
    }
}
