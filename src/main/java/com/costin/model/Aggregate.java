package com.costin.model;

public class Aggregate {

    private String serverName;
    private String minute;
    private Long count;
    private Long sum;
    private Double average;

    public Aggregate() {
    }

    public Aggregate(String serverName, String minute, Long count, Long sum, Double average) {
        this.serverName = serverName;
        this.minute = minute;
        this.count = count;
        this.sum = sum;
        this.average = average;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    @Override
    public String toString() {
        return "Aggregate{" +
                "serverName='" + serverName + '\'' +
                ", minute='" + minute + '\'' +
                ", count=" + count +
                ", sum=" + sum +
                ", average=" + average +
                '}';
    }
}
