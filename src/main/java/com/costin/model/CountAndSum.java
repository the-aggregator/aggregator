package com.costin.model;

public class CountAndSum {
    private Long count;
    private Long sum;
    private Long timestamp;

    public CountAndSum() {
        this.count = 0L;
        this.sum = 0L;
        this.timestamp = 0L;
    }

    public CountAndSum(Long count, Long sum, Long timestamp) {
        this.count = count;
        this.sum = sum;
        this.timestamp = timestamp;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getSum() {
        return sum;
    }

    public void setSum(Long sum) {
        this.sum = sum;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "CountAndSum{" +
                "count=" + count +
                ", sum=" + sum +
                ", timestamp=" + timestamp +
                '}';
    }
}
