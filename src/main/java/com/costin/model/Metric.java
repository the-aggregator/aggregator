package com.costin.model;

public class Metric {
    private Long timestamp;
    private Long value;

    public Metric() {
    }

    public Metric(Long timestamp, Long value) {
        this.timestamp = timestamp;
        this.value = value;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Metric{" +
                "timestamp='" + timestamp + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
